package com.groundgurus.landing.page.dao;

import com.groundgurus.landing.page.entities.Registration;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 *
 * @author pgutierrez
 */
@Stateless
public class RegistrationDao {
    @PersistenceContext
    private EntityManager em;
    
    @Transactional
    public void saveRegistration(Registration registration) {
        em.persist(registration);
    }
}
